use super::*;
use std::boxed::Box;

pub struct Jump {
    jump_if: bool,
    value: Parameter,
    jump_to: Parameter
}

impl Jump {
    pub fn from_program(position: usize, program: &mut Vec<i64>, param_modes: Vec<usize>) 
        -> Result<Box<dyn Executable>, String> {
        
        const CODE_TRUE: usize = 5;
        const CODE_FALSE: usize = 6;
        let raw_instruction = extract_raw_instruction(program[position]);
        let jump_if = match raw_instruction {
            CODE_TRUE => true,
            CODE_FALSE => false,
            _ => return Err("This is not a jump instruction".to_string())
        };

        if !check_remaining_space(position, 2, program) {
            return Err("Not enough instructions remaining".to_string());
        }

        let mut modes = param_modes;

        pad_modes(&mut modes, 2);

        Ok(Box::new(Jump {
            jump_if: jump_if, 
            value: Parameter::create(modes[1], program[position + 1]),
            jump_to: Parameter::create(modes[0], program[position + 2])
        }))
    }
}

impl Executable for Jump {
    fn execute(&self, program: &mut Vec<i64>, relative_base: &mut i64
        , _receiver: &Receiver<i64>, _sender: &Sender<Option<i64>>) -> FinalAction {

        let value = self.value.extract_value(program, *relative_base);

        if (value != 0 && self.jump_if) 
            || (value == 0 && !self.jump_if) {
                let target = self.jump_to.extract_value(program, *relative_base);
                return FinalAction::Jump(target as usize);
        }

        FinalAction::Increment(3)
    }
}