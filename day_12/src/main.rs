extern crate num;

use std::cmp::Ordering;
use std::cell::RefCell;
use num::integer::lcm;
use std::time::SystemTime;

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
struct Vec3 {
    x: i64,
    y: i64,
    z: i64
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
struct Moon {
    position: Vec3,
    velocity: Vec3
}

impl Vec3 {
    fn get_direction_vector(&self, other: &Self) -> Self {
        let x = match self.x.cmp(&other.x) {
            Ordering::Equal => 0,
            Ordering::Greater => -1,
            Ordering::Less => 1
        };

        let y = match self.y.cmp(&other.y) {
            Ordering::Equal => 0,
            Ordering::Greater => -1,
            Ordering::Less => 1
        };

        let z = match self.z.cmp(&other.z) {
            Ordering::Equal => 0,
            Ordering::Greater => -1,
            Ordering::Less => 1
        };

        Vec3{x:x, y:y, z:z}
    }

    fn translate(&mut self, other: &Self) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
    }
}

impl Moon {
    fn new(position: Vec3) -> Moon {
        let vel = Vec3{x:0, y:0, z:0};
        Moon{position: position, velocity: vel}
    }

    fn adjust_velocity(&mut self, other: &Self) {
        let dir = self.position.get_direction_vector(&other.position);
        self.velocity.translate(&dir);
    }

    fn move_moon(&mut self) {
        self.position.translate(&self.velocity);
    }

    fn calc_potential_energy(&self) -> i64 {
        let p = &self.position;
        p.x.abs() + p.y.abs() + p.z.abs()
    }

    fn calc_kinetic_energy(&self) -> i64 {
        let v = &self.velocity;
        v.x.abs() + v.y.abs() + v.z.abs()
    }

    fn calc_total_energy(&self) -> i64 {
        self.calc_potential_energy() 
            * self.calc_kinetic_energy()
    }
}

fn main() {
    let now = SystemTime::now();
    let moons = vec![
        RefCell::new(Moon::new(Vec3{x:5,y:-1,z:5})),
        RefCell::new(Moon::new(Vec3{x:0,y:-14,z:2})),
        RefCell::new(Moon::new(Vec3{x:16,y:4,z:0})),
        RefCell::new(Moon::new(Vec3{x:18,y:1,z:16}))
    ];
    part_1(moons.clone());
    part_2(moons);

    if let Ok(elapsed) = now.elapsed() {
        println!();
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn step(moons: &Vec<RefCell<Moon>>) {
    for i in 0..moons.len() {
        let mut m = moons[i].borrow_mut();
        for j in 0..moons.len() {
            if i != j {
                let o = moons[j].borrow();
                m.adjust_velocity(&*o);
            }
        }
    }

    for i in 0..moons.len() {
        let mut m = moons[i].borrow_mut();
        m.move_moon();
    }
}

fn part_1(moons: Vec<RefCell<Moon>>) {
    for _ in 0..1000 {
        step(&moons);   
    }

    let mut energy = 0;
    for i in 0..moons.len() {
        let m = moons[i].borrow();
        energy += m.calc_total_energy();
    }

    println!("Part 1: {}", energy);
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
struct CoordAndVelocity {
    coord: i64,
    vel: i64
}

fn state_for_x(moons: &Vec<RefCell<Moon>>) -> Vec<CoordAndVelocity> {
    let mut ret = Vec::new();
    for mu in moons {
        let m = mu.borrow();
        ret.push(CoordAndVelocity{coord: m.position.x, vel: m.velocity.x});
    }

    ret
}

fn state_for_y(moons: &Vec<RefCell<Moon>>) -> Vec<CoordAndVelocity> {
    let mut ret = Vec::new();
    for mu in moons {
        let m = mu.borrow();
        ret.push(CoordAndVelocity{coord: m.position.y, vel: m.velocity.y});
    }

    ret
}

fn state_for_z(moons: &Vec<RefCell<Moon>>) -> Vec<CoordAndVelocity> {
    let mut ret = Vec::new();
    for mu in moons {
        let m = mu.borrow();
        ret.push(CoordAndVelocity{coord: m.position.z, vel: m.velocity.z});
    }

    ret
}

fn compare_states(s1: &Vec<CoordAndVelocity>, s2: &Vec<CoordAndVelocity>) -> bool {
    s1.iter().zip(s2.iter()).find(|(a,b)| **a != **b).is_none()
}

fn part_2(moons: Vec<RefCell<Moon>>) {
    let mut count = 0;
    let state_x = state_for_x(&moons);
    let state_y = state_for_y(&moons);
    let state_z = state_for_z(&moons);
    let mut x_number: u64 = 0;
    let mut y_number: u64 = 0;
    let mut z_number: u64 = 0;
    loop {
        step(&moons);
        count += 1;
        if x_number == 0 {
            let new_x = state_for_x(&moons);

            if compare_states(&state_x, &new_x) {
                println!("Found x on cycle {}", count);
                x_number = count;
            }
        }
        if y_number == 0 {
            let new_y = state_for_y(&moons);

            if compare_states(&state_y, &new_y) {
                println!("Found y on cycle {}", count);
                y_number = count;
            }
        }
        if z_number == 0 {
            let new_z = state_for_z(&moons);

            if compare_states(&state_z, &new_z) {
                println!("Found z on cycle {}", count);
                z_number = count;
            }
        }

        if x_number != 0 && y_number != 0 && z_number != 0 {
            println!("Found all cycles: {},{},{}", x_number, y_number,z_number);
            let lcm1 = lcm(y_number, z_number);
            let  cm = lcm(x_number, lcm1);
            println!("Part 2: {}", cm);
            return;
        }
        
    }
}
