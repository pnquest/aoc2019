use std::fs;
use std::collections::{HashMap};
use std::time::SystemTime;

#[derive(PartialEq, Eq, Hash, Clone)]
struct Chemical {
    qty: u64,
    name: String
}

impl Chemical {
    fn resolve(&self, qty: u64, book: &HashMap<String,Recipie>, stockpile: &mut HashMap<String,u64>) -> u64 {
        let mut ore_count = 0;
        let mut qty_made_so_far = 0;
        let qty_made_per_round = book[&self.name].output.qty;
        let requires = book[&self.name].inputs.clone();
        //check the stockpile for this chemical and take any surplus
        let ent = stockpile.entry(self.name.clone()).or_insert(0);
        if *ent > 0 {
            if *ent >= qty {
                qty_made_so_far = qty;
                *ent -= qty;
            } else {
                qty_made_so_far = *ent;
                *ent = 0;
            }
        }
        let multiplier = ((qty - qty_made_so_far) as f64 / qty_made_per_round as f64).ceil() as u64;
        //if we need more, we need to make it
        while qty_made_so_far < qty {
            for chem in &requires {
                //if the input is ore, just take it
                if chem.name == "ORE" {
                    ore_count += chem.qty * multiplier;
                } else { //otherwise we need to make it
                    let ent = stockpile.entry(chem.name.clone()).or_insert(0);
                    //if we have enough in the stockpile, then take it and move on
                    if *ent >= chem.qty * multiplier {
                        *ent -= chem.qty * multiplier;
                        continue;
                    } else{ //otherwise, we need to make it
                        ore_count += chem.resolve(chem.qty * multiplier, book, stockpile);
                    }
                }
            }
            qty_made_so_far += qty_made_per_round * multiplier;
        }
        if qty_made_so_far > qty {
            let stocked = qty_made_so_far - qty;
            let ent  = stockpile.entry(self.name.clone()).or_insert(0);
            *ent += stocked;
        }
        ore_count
    }
}

struct Recipie {
    inputs: Vec<Chemical>,
    output: Chemical
}

fn parse_chemical(input: &str) -> Chemical {
    let qtyidx = input.find(" ").unwrap();
    let qty: u64 = input[0..qtyidx].parse().unwrap();
    let name: String = input[qtyidx+1..].to_string();
    Chemical{qty:qty, name:name}
}

fn main() {
    let now = SystemTime::now();
    let file = fs::read_to_string("./input.txt").expect("Could not read file");
    let mut book = HashMap::new();
    for line in file.lines() {
        let splits = line.split("=>");
        let mut is_first = true;
        let mut inputs = Vec::new();
        for split in splits {
            if is_first {
                is_first = false;
                let splt2 = split.trim().split(", ");
                for splt in splt2 {
                    let chem = parse_chemical(&splt);
                    inputs.push(chem);
                }
            } else {
                let chem = parse_chemical(&split.trim());
                inputs.insert(0, chem);
            }
        }
        let output = inputs.remove(0);
        book.insert(output.name.clone(), Recipie{inputs:inputs, output:output});
    }

    part_1(&book);
    part_2(&book);

    if let Ok(elapsed) = now.elapsed() {
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn part_1(book: &HashMap<String, Recipie>) {
    let mut stockpile = HashMap::new();
    let fuel = &book["FUEL"];
    let ore_used = fuel.output.resolve(1, &book, &mut stockpile);
    println!("Part 1: {}", ore_used);
}

fn part_2(book: &HashMap<String, Recipie>) {
    const ORIGINAL_ORE: u64 = 1000000000000;
    let mut cur_guess: u64 = ORIGINAL_ORE / 2;
    let mut top_interval: u64 = ORIGINAL_ORE;
    let mut bottom_interval: u64 = 0;
    let mut last_low_guess = 0;
    let fuel = &book["FUEL"];
    loop {
        let mut stockpile = HashMap::new();
        let used = fuel.output.resolve(cur_guess, &book, &mut stockpile);

        if top_interval - bottom_interval == 0 {
            println!("Part 2: {}", cur_guess);
            return;
        }
        if used < ORIGINAL_ORE {
            if cur_guess > last_low_guess {
                last_low_guess = cur_guess;
                bottom_interval = cur_guess;
            }
            cur_guess = bottom_interval + (top_interval - bottom_interval) / 2;
        } else {
            top_interval = cur_guess - 1;
            cur_guess =  top_interval - (top_interval - bottom_interval) / 2;
        }
    }
    
}