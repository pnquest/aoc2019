use std::cmp::{Ordering, min, max};

#[derive(Eq, PartialEq, Hash, Clone, Copy, Debug)]
pub struct Point {
    pub x: isize,
    pub y: isize
}

pub struct Line {
    pub p1: Point,
    pub p2: Point
}

#[derive(PartialEq)]
pub enum Direction {
    Colinear,
    Clockwise,
    AntiClockwise
}

impl Ord for Point {
    fn cmp(&self, other: &Point) -> Ordering {
        let cy = self.y.cmp(&other.y);
        match cy {
            Ordering::Equal => self.x.cmp(&other.x),
            _ => cy
        }
    }
}

impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Point) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Point {
    pub fn manhattan_distance(&self, other: &Point) -> usize {
        let res = (self.x - other.x).abs() + (self.y - other.y).abs();
        res as usize
    }

    pub fn distance(&self, other: &Point) -> f64 {
        let pow = (self.x - other.x).pow(2) + (self.y - other.y).pow(2);
        let pow = pow as f64;
        pow.sqrt()
    }
    
    pub fn direction(self, b: Point, c: Point) -> Direction {
        let val = (b.y-self.y)*(c.x-b.x)-(b.x-self.x)*(c.y-b.y);

        if val == 0 {
            Direction::Colinear
        }else if val < 0 {
            Direction::AntiClockwise
        } else {
            Direction::Clockwise
        }
    }

    pub fn translate(&self, x: isize, y: isize) -> Point {
        Point{x: self.x + x, y: self.y + y}
    }
}

pub enum Slope {
    Vertical,
    Horizontal,
    Sloped(f64)
}

impl Line {

    pub fn from_points(p1: Point, p2: Point) -> Line {
        Line{p1: p1, p2: p2}
    }

    pub fn from_coords(x1: isize, y1: isize, x2: isize, y2: isize) -> Line {
        let p1 = Point{x: x1, y: y1};
        let p2 = Point{x: x2, y: y2};
        Line::from_points(p1, p2)
    }

    pub fn length(&self) -> f64 {
        let x = (self.p1.x - self.p2.x).pow(2) as f64;
        let y = (self.p1.y - self.p2.y).pow(2) as f64;

        (x + y).sqrt()
    }

    pub fn distance_along_line(&self, target: Point) -> f64 {
        if !self.is_on_line(target) {
            panic!("target point is not on line");
        }

        let tmp = Line::from_points(self.p1, target);
        tmp.length()
    }

    pub fn slope(&self) -> Slope {
        let x = self.p2.x - self.p1.x;
        let y = self.p2.y - self.p1.y;
        if x == 0 {
            return Slope::Vertical;
        }
        if y == 0 {
            return Slope::Horizontal
        }
        Slope::Sloped((self.p1.x - self.p2.x) as f64 / (self.p1.y - self.p2.y) as f64)
    }

    pub fn angle(&self, other: Line) -> f64 {
        let l_ab = self.length();
        let l_ac = other.length();
        let l_bc = self.p2.distance(&other.p2);
        let cos = (l_ab.powi(2) + l_ac.powi(2) - l_bc.powi(2)) / (2.0 * l_ab * l_ac);
        let mut rads = cos.acos();
        if self.p2.x > other.p2.x {
             rads  = 2.0 * std::f64::consts::PI - rads;
        }
        rads
    }

    pub fn is_on_line(&self, p: Point) -> bool {
        let slp1 = self.slope();
        let slp2 = Line::from_points(self.p1, p).slope();

        const INF: f64 = 9999999999999999999999.0;
        
        let s1 = match slp1 {
            Slope::Horizontal => 0.0,
            Slope::Vertical => INF,
            Slope::Sloped(v) => v
        };

        let s2 = match slp2 {
            Slope::Horizontal => 0.0,
            Slope::Vertical => INF,
            Slope::Sloped(v) => v
        };

        if s1 != s2 {
            return false;
        }

        p.x >= min(self.p1.x, self.p2.x)
        && p.x <= max(self.p1.x, self.p2.x)
        && p.y >= min(self.p1.y, self.p2.y)
        && p.y <= max(self.p1.y, self.p2.y)
    }

    pub fn does_intersect(&self, other: &Line) -> bool {
        let dir1 = self.p1.direction(self.p2, other.p1);
        let dir2 = self.p1.direction(self.p2, other.p2);
        let dir3 = other.p1.direction(other.p2, self.p1);
        let dir4 = other.p1.direction(other.p2, self.p2);

        if dir1 != dir2 && dir3 != dir4 {
            return true;
        } 
        else if dir1 == Direction::Colinear && self.is_on_line(other.p1) {
            return true;
        }
        else if dir2 == Direction::Colinear && self.is_on_line(other.p2) {
            return true;
        }
        else if dir3 == Direction::Colinear && other.is_on_line(self.p1) {
            return true;
        }
        else if dir4 == Direction::Colinear && other.is_on_line(self.p2) {
            return true;
        }

        false
    }

    pub fn get_intersection(&self, other: &Line) -> Option<Point> {
        let a1 = self.p2.y - self.p1.y;
        let b1 = self.p1.x - self.p2.x;
        let c1 = a1 * self.p1.x + b1 * self.p1.y;
 
        let a2 = other.p2.y - other.p1.y;
        let b2 = other.p1.x - other.p2.x;
        let c2 = a2 * other.p1.x + b2 * other.p1.y;
 
        let delta = a1 * b2 - a2 * b1;
 
        if delta == 0 {
            return None;
        }
 
        Some(Point {
            x: (b2 * c1 - b1 * c2) / delta,
            y: (a1 * c2 - a2 * c1) / delta,
        })
    }
}

#[derive(PartialEq, PartialOrd)]
pub struct Rectangle {
    pub left: usize,
    pub top: usize,
    pub width: usize,
    pub height: usize,
}

impl Rectangle {
    pub fn new(left: usize, top: usize, width: usize, height: usize) -> Rectangle {
        Rectangle {
            left: left,
            top: top,
            width: width,
            height: height,
        }
    }

    pub fn get_area(&self) -> usize {
        self.width * self.height
    }

    pub fn get_right(&self) -> usize {
        self.left + self.width
    }

    pub fn get_bottom(&self) -> usize {
        self.top + self.height
    }

    pub fn intersect(&self, other: &Rectangle) -> Option<Rectangle> {
        let mut left = 0;
        let mut top = 0;
        let mut width = 0;
        let mut height = 0;

        //if other contains self on the horizontal plane
        if self.left >= other.left && self.left <= other.get_right() {
            left = self.left;

            if self.get_right() <= other.get_right() {
                width = self.width;
            } else {
                width = other.get_right() - self.left;
            }
        }
        //if self contains other on the horizontal plane
        else if other.left >= self.left && other.left <= self.get_right() {
            left = other.left;

            if other.get_right() <= self.get_right() {
                width = other.width;
            } else {
                width = self.get_right() - other.left;
            }
        }

        //if other contains self on the vertical plane
        if self.top >= other.top && self.top <= other.get_bottom() {
            top = self.top;

            if self.get_bottom() <= other.get_bottom() {
                height = self.height;
            } else {
                height = other.get_bottom() - self.top;
            }
        } else if other.top >= self.top && other.top <= self.get_bottom() {
            top = other.top;

            if other.get_bottom() <= self.get_bottom() {
                height = other.height;
            } else {
                height = self.get_bottom() - other.top;
            }
        }

        if width == 0 || height == 0 {
            return None;
        }

        Some(Rectangle::new(left, top, width, height))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_area() {
        let r = Rectangle::new(0,0,2,3);
        let area = r.get_area();

        assert_eq!(area, 6);
    }

    #[test]
    fn test_right() {
        let r = Rectangle::new(0,0,2,3);
        let right = r.get_right();

        assert_eq!(right, 2);
    }

    #[test]
    fn test_bottom() {
        let r = Rectangle::new(0,0,2,3);
        let bottom = r.get_bottom();

        assert_eq!(bottom, 3);
    }

    #[test]
    fn test_intersect_none() {
        let r = Rectangle::new(0, 0, 2, 3);
        let r2 = Rectangle::new(5,5,2,3);

        let over = match r.intersect(&r2){
            Some(_) => true,
            None => false
        };

        assert_eq!(false, over);

        let over2 = match r2.intersect(&r){
            Some(_) => true,
            None => false
        };

        assert_eq!(false, over2);
    }

    #[test]
    fn test_intersect_left_partial() {
        let r = Rectangle::new(2, 2, 5, 5);
        let r2 = Rectangle::new(0,0,5,5);

        let over = r.intersect(&r2).unwrap();

        assert_eq!(2, over.left);
        assert_eq!(2, over.top);
        assert_eq!(3, over.width);
        assert_eq!(3, over.height);

        let over2 = r2.intersect(&r).unwrap();

        assert_eq!(2, over2.left);
        assert_eq!(2, over2.top);
        assert_eq!(3, over2.width);
        assert_eq!(3, over2.height);
    }

    #[test]
    fn test_intersect_left_full() {
        let r = Rectangle::new(2, 2, 2, 2);
        let r2 = Rectangle::new(0,0,5,5);

        let over = r.intersect(&r2).unwrap();

        assert_eq!(2, over.left);
        assert_eq!(2, over.top);
        assert_eq!(2, over.width);
        assert_eq!(2, over.height);

        let over2 = r2.intersect(&r).unwrap();

        assert_eq!(2, over2.left);
        assert_eq!(2, over2.top);
        assert_eq!(2, over2.width);
        assert_eq!(2, over2.height);
    }

    #[test]
    fn test_line_intersect_false() {
        let l1 = Line::from_coords(0, 0, 1006, 0);
        let l2 = Line::from_coords(-1000, 0, -1000, -65);

        let res = l1.does_intersect(&l2);

        assert!(!res);
    }
}