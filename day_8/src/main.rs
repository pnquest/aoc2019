use std::fs::read_to_string;
use std::collections::HashMap;
use std::time::SystemTime;

struct Layer {
    width: usize,
    height: usize,
    cur_width: usize,
    cur_height: usize,
    values: Vec<Vec<usize>>
}

impl Layer {
    fn new(width: usize, height: usize) -> Layer {
        Layer{width: width, height: height, cur_width: 0, cur_height: 0, values: vec![Vec::new();height]}
    }

    fn add(&mut self, value: usize) -> Result<bool, String> {
        if self.cur_width == self.width && self.cur_height == self.height {
            return Err("This layer is already full".to_string());
        }

        self.values[self.cur_height].push(value);
        self.cur_width += 1;

        if self.cur_width == self.width {
            self.cur_height += 1;
            self.cur_width = 0;

            if self.cur_height == self.height {
                return Ok(false);
            }
        }

        Ok(true)
    }

    fn is_full(&self) -> bool {
        self.height == self.cur_height
    }

    fn get_digit_counts(&self) -> HashMap<usize, usize> {
        let mut ret = HashMap::new();
        for row in 0..self.height {
            for col in 0..self.width {
                let v = self.values[row][col];
                let ent = ret.entry(v).or_insert(0);
                *ent += 1;
            }
        }

        ret
    }

    fn get_digit_at(&self, width: usize, height: usize) -> usize {
        self.values[height][width]
    }
}

fn main() {
    let now = SystemTime::now();
    const WIDTH: usize = 25;
    const HEIGHT: usize = 6;


    let input = read_to_string("./input.txt").expect("error reading file");
    let mut layers = Vec::new();
    let mut cur_layer = Layer::new(WIDTH, HEIGHT);
    for c in input.chars() {
        if cur_layer.is_full() {
            layers.push(cur_layer);
            cur_layer = Layer::new(WIDTH, HEIGHT);
        }

        let v = c.to_digit(10).unwrap();

        cur_layer.add(v as usize).expect("could not add value");
    }
    //push the last layer
    layers.push(cur_layer);

    println!("Layer count is {}", layers.len());

    part_1(&layers);
    part_2(&layers, HEIGHT, WIDTH);

    if let Ok(elapsed) = now.elapsed() {
        println!();
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn part_2(layers: &Vec<Layer>, height: usize, width: usize) {
    println!();
    for h in 0..height {
        for w in 0..width {
            let mut cur_val = 2;
            for l in layers {
                if cur_val == 2 {
                    let v = l.get_digit_at(w, h);
                    cur_val = v;
                }
            }

            if cur_val != 0 {
                print!("{}", "*");
            } else {
                print!("{}", " ");
            }
            
        }
        println!("");
    }
}

fn part_1(layers: &Vec<Layer>) {
    let mut zero_count = 99999999999;
    let mut smallest_layer = None;

    for layer in layers {
        let counts = layer.get_digit_counts();
        let cnt = *counts.get(&0).unwrap_or(&0);

        if cnt < zero_count {
            zero_count = cnt;
            smallest_layer = Some(counts);
        }
    }

    let l = smallest_layer.unwrap();
    let result = *l.get(&1).unwrap_or(&0) * *l.get(&2).unwrap_or(&0);
    println!("Part 1: {}", result);
}