use super::*;
use std::boxed::Box;

pub struct Multiplier {
    left_idx: Parameter,
    right_idx: Parameter,
    target_idx: Parameter
}

impl Multiplier {
    pub fn from_program(position: usize, program: &mut Vec<i64>, param_modes: Vec<usize>) 
        -> Result<Box<dyn Executable>, String> {
        
        const CODE: usize = 2;
        if extract_raw_instruction(program[position]) != CODE {
            return Err("This is not a multiplication instruction".to_string());
        }

        if !check_remaining_space(position, 3, program) {
            return Err("Not enough instructions remaining".to_string());
        }

        let mut modes = param_modes;

        pad_modes(&mut modes, 3);

        Ok(Box::new(Multiplier{
            left_idx: Parameter::create(modes[2], program[position + 1]), 
            right_idx: Parameter::create(modes[1], program[position + 2]), 
            target_idx: Parameter::create(modes[0], program[position + 3])
        }))
    }
}

impl Executable for Multiplier {
    fn execute(&self, program: &mut Vec<i64>, relative_base: &mut i64,
        _receiver: &Receiver<i64>, _sender: &Sender<Option<i64>>) -> FinalAction {

        let t = self.target_idx.extract_address(program, *relative_base);
        program[t] = self.left_idx.extract_value(program, *relative_base) 
            * self.right_idx.extract_value(program, *relative_base);

        FinalAction::Increment(4)
    }
}