pub struct Combinations<'a, T> {
    values: &'a Vec<T>,
    cur: Vec<usize>,
    fields: usize,
    max_index: usize,
    has_started: bool
}

pub fn combinations<'a, T>(values: &'a Vec<T>, fields: usize) -> Combinations<'a, T> {
    Combinations{
        values: values,
        fields: fields,
        cur: vec![0;fields], 
        max_index: values.len() - 1,
        has_started: false
    }
}

impl<'a, T> Iterator for Combinations<'a, T> {
    type Item = Vec<&'a T>;

    fn next(&mut self) -> Option<Vec<&'a T>> {
        if !self.has_started {
            self.has_started = true;
        } else {
            for i in (0..self.fields).rev()  {
                //we are done
                if i == 0 && self.cur[i] == self.max_index {
                    return None;
                }
    
                if self.cur[i] == self.max_index {
                    self.cur[i] = 0;
                } else {
                    self.cur[i] += 1;
                    break;
                }
            }
        }

        let mut ret = Vec::new();
        for i in 0..self.fields {
            ret.push(&self.values[self.cur[i]]);
        }

        
        Some(ret)
    }
}

#[derive(Clone)]
pub struct ExpansionIterator<T> where T: Iterator+Clone, T::Item: Clone{
    current: Option<T::Item>,
    cur_count: usize,
    max_count: usize,
    source: T,
}

impl<T> ExpansionIterator<T> where T: Iterator+Clone, T::Item: Clone{
    pub fn expand(count: usize, iter: T) -> ExpansionIterator<T> {
        ExpansionIterator{
            current: None,
            cur_count: 0,
            max_count: count,
            source: iter
        }
    }
}

impl<T> Iterator for ExpansionIterator<T> where T: Iterator+Clone, T::Item: Clone {
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current.is_none() || self.cur_count == self.max_count {
            match self.source.next() {
                Some(v) => self.current = Some(v),
                None => return None
            };

            self.cur_count = 0;
        }

        self.cur_count += 1;
        self.current.clone()

    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_iterate() {
        let values = vec![0,1,2];

        let expected = vec![
            vec![0,0,0],
            vec![0,0,1],
            vec![0,0,2],
            vec![0,1,0],
            vec![0,1,1],
            vec![0,1,2],
            vec![0,2,0],
            vec![0,2,1],
            vec![0,2,2],
            vec![1,0,0],
            vec![1,0,1],
            vec![1,0,2],
            vec![1,1,0],
            vec![1,1,1],
            vec![1,1,2],
            vec![1,2,0],
            vec![1,2,1],
            vec![1,2,2],
            vec![2,0,0],
            vec![2,0,1],
            vec![2,0,2],
            vec![2,1,0],
            vec![2,1,1],
            vec![2,1,2],
            vec![2,2,0],
            vec![2,2,1],
            vec![2,2,2]
        ];

        let mut i = 0;

        for c in combinations(&values, 3){
            for k in 0..3 {
                assert_eq!(*c[k], expected[i][k]);
            }
            i += 1;
        }
    }

    #[test]
    fn can_expand() {
        let val = vec![0,1,2];
        let expected = vec![0,0,1,1,2,2];

        let exp = ExpansionIterator::expand(2, Box::new(val.iter()));

        for e in expected {
            let v = exp.next().unwrap();
            assert_eq!(*v,e);
        }

        assert_eq!(true, exp.next().is_none());
    }
}