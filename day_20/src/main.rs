extern crate common;

use common::geometry::Point;
use common::pathfinding::*;

use std::collections::{BTreeMap, HashMap};
use std::fs::read_to_string;

enum SpaceType {
    Wall,
    Open,
    Teleport(String, Point),
}

struct TeleporterMapping {
    p1: Option<Point>,
    p2: Option<Point>,
}

impl TeleporterMapping {
    fn get_other(&self, p: Point) -> Option<Point> {
        if let Some(p1) = self.p1 {
            if p == p1 {
                return self.p2;
            }
        }

        if let Some(p2) = self.p2 {
            if p == p2 {
                return self.p1;
            }
        }

        None
    }
}

fn main() {
    let input = read_to_string("input.txt").expect("could not read file");
    let char_map = get_char_map(input);
    let max_y = char_map.len();
    let max_x = char_map[0].len();
    let mut teleports = HashMap::new();
    let mut map = BTreeMap::new();

    for y in 0..char_map.len() {
        for x in 0..char_map[y].len() {
            let point = convert_char(x, y, max_x, max_y, &char_map, &mut teleports);
            map.insert(
                Point {
                    x: x as isize,
                    y: y as isize,
                },
                point,
            );
        }
    }

    let (mp, start, goal) = convert_map(&mut teleports, &map);

    let res = solve_map(&mp, start, goal);

    if let Some(r) = res {
        for rp in r.iter().rev() {
            println!("{:?}", rp);
        }
        println!("Part 1: {}", r.len() - 1);
    } else {
        println!("That is why you failed");
    }
}

fn convert_teleport(
    sp: &SpaceType,
    start: &mut Point,
    goal: &mut Point,
    teleports: &mut HashMap<String, TeleporterMapping>,
) -> Space {
    if let SpaceType::Teleport(n, p) = sp {
        if n == "AA" {
            *start = *p;
            return Space::Start(*p);
        } else if n == "ZZ" {
            *goal = *p;
            return Space::Goal(*p);
        } else {
            let ent = teleports
                .entry(n.clone())
                .or_insert(TeleporterMapping { p1: None, p2: None });
            if let Some(dest) = ent.get_other(*p) {
                return Space::Open(dest);
            } else {
                panic!("Could not find twin!");
            }
        }
    }

    panic!("Not a teleprot!");
}

fn convert_map(
    teleports: &mut HashMap<String, TeleporterMapping>,
    map: &BTreeMap<Point, SpaceType>,
) -> (Map, Point, Point) {
    let mut start = Point { x: -1, y: -1 };
    let mut goal = Point { x: -1, y: -1 };
    let mut ret = BTreeMap::new();
    for (p, s) in map {
        let sp = match s {
            SpaceType::Wall => Space::Blocked(*p),
            SpaceType::Open => Space::Open(*p),
            SpaceType::Teleport(_, _) => convert_teleport(s, &mut start, &mut goal, teleports),
        };
        ret.insert(*p, sp);
    }
    (ret, start, goal)
}

fn handle_teleporter_space(
    x: usize,
    y: usize,
    c: char,
    max_x: usize,
    max_y: usize,
    char_map: &Vec<Vec<char>>,
    teleports: &mut HashMap<String, TeleporterMapping>,
) -> SpaceType {
    let char_above = if y > 0 {
        Some(char_map[y - 1][x])
    } else {
        None
    };

    let char_below = if y < max_y - 1 {
        Some(char_map[y + 1][x])
    } else {
        None
    };

    let char_left = if x > 0 {
        Some(char_map[y][x - 1])
    } else {
        None
    };

    let char_right = if x < max_x - 1 {
        Some(char_map[y][x + 1])
    } else {
        None
    };

    let spc = if char_above.is_some()
        && char_above.unwrap().is_ascii_uppercase()
        && char_below.is_some()
        && char_below.unwrap() == '.'
    {
        let above = char_above.unwrap();
        let mut name_string = String::new();
        name_string.push(above);
        name_string.push(c);
        SpaceType::Teleport(
            name_string,
            Point {
                x: x as isize,
                y: (y + 1) as isize,
            },
        )
    } else if char_below.is_some()
        && char_below.unwrap().is_ascii_uppercase()
        && char_above.is_some()
        && char_above.unwrap() == '.'
    {
        let below = char_below.unwrap();
        let mut name_string = String::new();
        name_string.push(c);
        name_string.push(below);
        SpaceType::Teleport(
            name_string,
            Point {
                x: x as isize,
                y: (y - 1) as isize,
            },
        )
    } else if char_left.is_some()
        && char_left.unwrap().is_ascii_uppercase()
        && char_right.is_some()
        && char_right.unwrap() == '.'
    {
        let left = char_left.unwrap();
        let mut name_string = String::new();
        name_string.push(left);
        name_string.push(c);
        SpaceType::Teleport(
            name_string,
            Point {
                x: (x + 1) as isize,
                y: y as isize,
            },
        )
    } else if char_right.is_some()
        && char_right.unwrap().is_ascii_uppercase()
        && char_left.is_some()
        && char_left.unwrap() == '.'
    {
        let right = char_right.unwrap();
        let mut name_string = String::new();
        name_string.push(c);
        name_string.push(right);
        SpaceType::Teleport(
            name_string,
            Point {
                x: (x - 1) as isize,
                y: y as isize,
            },
        )
    } else {
        SpaceType::Wall
    };

    if let SpaceType::Teleport(s, p) = &spc {
        update_teleporter_mapping(p.x, p.y, s.clone(), teleports);
    }

    spc
}

fn convert_char(
    x: usize,
    y: usize,
    max_x: usize,
    max_y: usize,
    char_map: &Vec<Vec<char>>,
    teleports: &mut HashMap<String, TeleporterMapping>,
) -> SpaceType {
    let c = char_map[y][x];
    match c {
        ' ' | '#' => SpaceType::Wall,
        '.' => SpaceType::Open,
        'A'..='Z' => handle_teleporter_space(x, y, c, max_x, max_y, char_map, teleports),
        _ => panic!("bad char"),
    }
}

fn update_teleporter_mapping(
    x: isize,
    y: isize,
    teleport_name: String,
    teleports: &mut HashMap<String, TeleporterMapping>,
) {
    let ent = teleports
        .entry(teleport_name)
        .or_insert(TeleporterMapping { p1: None, p2: None });
    if ent.p1.is_none() {
        ent.p1 = Some(Point { x: x, y: y });
    } else {
        ent.p2 = Some(Point { x: x, y: y });
    }
}

fn get_char_map(input: String) -> Vec<Vec<char>> {
    let mut char_map: Vec<Vec<char>> = Vec::new();
    let mut y = 0;
    for l in input.lines() {
        char_map.push(Vec::new());
        for c in l.chars() {
            char_map[y].push(c);
        }

        y += 1;
    }

    char_map
}
