extern crate common;
use common::int_computer::{Computer, InputReference, OutputReference, VectorInputProvider, VectorOutputProvider};
use common::iter::combinations;
use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashSet;
use std::time::SystemTime;

struct Amplifier {
    computer: Computer,
    input: InputReference,
    output: OutputReference,
    is_running: bool,
    output_index: usize
}

impl Amplifier {
    fn reset(&mut self) {
        let mut out = self.output.borrow_mut();
        out.reset();
        self.computer.reset();
        self.is_running = false;
        self.output_index = 0;
    }

    fn resume(&mut self, value: isize) -> Option<isize> {
        let mut inp = self.input.borrow_mut();
        inp.push_input(value);
        drop(inp);

        self.computer.run_computer();
        let out = self.output.borrow();
        let res = out.get_output();
        if res.len() - 1 == self.output_index {
            let idx = self.output_index;
            self.output_index += 1;
            Some(res[idx])
        } else {
            None
        }
    }

    fn run(&mut self, phase: isize, value: isize) -> Option<isize> {
        self.is_running = true;
        let mut inp = self.input.borrow_mut();
        inp.set_input(vec![phase, value]);
        drop(inp);

        self.computer.run_computer();
        let out = self.output.borrow();
        let res = out.get_output();
        if res.len() - 1 == self.output_index {
            let idx = self.output_index;
            self.output_index += 1;
            Some(res[idx])
        } else {
            None
        }
    }

    fn new(program: Vec<isize>) -> Amplifier {
        let inbox = Box::new(VectorInputProvider::new());
        let inp: InputReference = Rc::new(RefCell::new(inbox));
        let outp: OutputReference = Rc::new(RefCell::new(Box::new(VectorOutputProvider::new())));
        let cmp = Computer::from_vec_with_providers(program, &inp, &outp);
        Amplifier{computer: cmp, input: Rc::clone(&inp), output: Rc::clone(&outp), is_running: false, output_index: 0}
    }
}

fn main() {
    let now = SystemTime::now();
    let program = vec![3,8,1001,8,10,8,105,1,0,0,21,34,51,64,81,102,183,264,345,426,99999,3,9,102,2,9,9,1001,9,4,9,4,9,
    99,3,9,101,4,9,9,102,5,9,9,1001,9,2,9,4,9,99,3,9,101,3,9,9,1002,9,5,9,4,9,99,3,9,102,3,9,9,101,3,9,9,1002,9,4,9,4,9,
    99,3,9,1002,9,3,9,1001,9,5,9,1002,9,5,9,101,3,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,
    101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,
    9,4,9,99,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,
    4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,
    3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,
    9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,
    9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,
    4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,
    1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,99];

    let mut amps = vec![Amplifier::new(program.clone()), Amplifier::new(program.clone()), Amplifier::new(program.clone()),
        Amplifier::new(program.clone()), Amplifier::new(program.clone())
    ];

    part_1(&mut amps);
    part_2(&mut amps);
    if let Ok(elapsed) = now.elapsed() {
        println!("run time: {}ms", elapsed.as_millis());
    }
}

fn part_2(amps: &mut Vec<Amplifier>) {
    let phases = vec![5, 6, 7, 8, 9];
    let mut max = 0;
    let mut max_phases = Vec::new();

    for phs in combinations(&phases, 5) {
        let deduped: HashSet<_> = phs
            .iter()
            .map(|p| *p)
            .collect();

        if phs.len() != deduped.len() {
            continue;
        }

        let mut index = 0;
        let mut inp = 0;

        for amp in &mut amps.iter_mut() {
            amp.reset();
        }

        loop {
            let amp = &mut amps[index % 5];
            let phase = *phs[index % 5];
            if amp.is_running {
                if let Some(v) = amp.resume(inp) {
                    inp = v;
                    index += 1;
                } else {
                    break;
                }
            } else {
                if let Some(v) = amp.run(phase, inp) {
                    inp = v;
                    index += 1;
                } else {
                    break;
                }
            }
            
        }

        if inp > max {
            max = inp;
            max_phases = phs.clone();
        }
    }
    println!("Part 2: {} ({:?})", max, max_phases);
}

fn part_1(amps: &mut Vec<Amplifier>) {
    let phases = vec![0, 1, 2, 3, 4];
    let mut max = 0;
    let mut max_phases = Vec::new();
    for phs in combinations(&phases, 5) {
        let deduped: HashSet<_> = phs
            .iter()
            .map(|p| *p)
            .collect();

        //dump out if the values are not distinct
        if phs.len() != deduped.len() {
            continue;
        }

        let mut inp = 0;
        let mut idx = 0;
        for amp in amps.iter_mut() {
            amp.reset();
            inp = amp.run(*phs[idx], inp).unwrap();
            idx += 1;
        }
        if inp > max {
            max = inp;
            max_phases = phs.clone();
        }
    }

    println!("Part 1: {} ({:?})", max, max_phases);
}
