extern crate common;

use common::geometry::{Line, Point};
use std::fs::read_to_string;
use std::collections::HashMap;
use std::time::SystemTime;

fn main() {
    let now = SystemTime::now();
    let input = read_to_string("./input.txt")
        .expect("could not read file");

    let mut asteroids = Vec::new();

    let mut y = 0;
    for l in input.lines() {
        let mut x = 0;
        for c in l.chars() {
            if c == '#' {
                asteroids.push(Point{x: x, y:y});
            }
            x += 1;
        }
        y += 1;
    }

    let selected = part_1(&asteroids);
    part_2(selected, &asteroids);

    if let Ok(elapsed) = now.elapsed() {
        println!();
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn part_2(selected: Point, asteroids: &Vec<Point>) {
    let up = Line::from_coords(selected.x, selected.y, selected.x, 0);
    let mut angles: Vec<(Point, f64)> = asteroids
        .iter()
        .filter(|a| **a != selected)
        .map(|a| (*a, (up.angle(Line::from_points(selected, *a)).to_degrees() * 100.0).round() / 100.0))
        .collect();
    angles.sort_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap());
    
    let mut prev = 0.0;
    let mut min_dist = std::f64::INFINITY;
    let mut removed = Vec::new();
    let mut min_point = selected;

    for (p, a) in angles {
        if a != prev {
            if !min_dist.is_infinite() {
                removed.push(min_point);

                if removed.len() == 200 {
                    println!("Part 2: ({},{}) = {}", min_point.x, min_point.y, min_point.x * 100 + min_point.y);
                    return;
                }
            }
            prev = a;
            min_dist = selected.distance(&p);
            min_point = p;
        }
        else {
            let dist = selected.distance(&p);
            if dist < min_dist {
                min_dist = dist;
                min_point = p;
            }
        }
    }
}

fn part_1(asteroids: &Vec<Point>) -> Point {
    let mut map = HashMap::new();
    for line in asteroids.iter()
        .flat_map(|a| asteroids.iter().filter(|b| **b != *a).map(|b| Line::from_points(*a, *b)).collect::<Vec<_>>()) {
        let mut found = false;
        for a in asteroids.iter().filter(|f| **f != line.p1 && **f != line.p2) {
            if line.is_on_line(*a) {
                found = true;
                break;
            }
        }

        if !found {
            let ent = map.entry(line.p1).or_insert(0);
            *ent += 1;
        }
    }

    let (a, count) = map.iter().max_by_key(|(_, v)| *v).unwrap();

    println!("Part 1: ({}, {}) can see {} other points", a.x, a.y, count);
    *a
}