extern crate common;
use common::geometry::{Point, Line};
use std::fs;
use std::time::SystemTime;

fn parse_wires(values: &str) -> Vec<Vec<Line>> {
    let mut wires = Vec::new();
    
    for l in values.lines() {
        let mut wire = Vec::new();
        let mut cur_pt = Point{x: 0, y: 0};
       for step in l.split(",") {
           let nxt = match &step[0..1] {
               "U" => cur_pt.translate(0, step[1..].parse().unwrap()),
               "D" => cur_pt.translate(0, step[1..].parse::<isize>().unwrap() * -1),
               "L" => cur_pt.translate(step[1..].parse::<isize>().unwrap() * -1, 0),
               "R" => cur_pt.translate(step[1..].parse().unwrap(), 0),
               _ => panic!("something bad")
           };

           wire.push(Line::from_points(cur_pt, nxt));
           cur_pt = nxt;
       }
       wires.push(wire);
    }

    wires
}

fn part_2(wires: &Vec<Vec<Line>>) {
    let wire1 = &wires[0];
    let wire2 = &wires[1];

    let mut best_point = None;
    let mut best_distance = 9999999999.0;
    const BASE: Point = Point{x: 0, y: 0};
    let mut w1_length = 0.0;
    let mut w2_length;
    for w1 in wire1 {
        w2_length = 0.0;
        for w2 in wire2 {
            if w1.does_intersect(&w2) {
                if let Some(pt) = w1.get_intersection(&w2) {
                    if pt != BASE {
                        let distance = w1_length 
                            + w1.distance_along_line(pt) 
                            + w2_length 
                            + w2.distance_along_line(pt);
                        if distance < best_distance {
                            best_point = Some(pt);
                            best_distance = distance;
                        }
                    }
                }
            }
            w2_length += w2.length();
        }
        w1_length += w1.length();
    }
    let pt = best_point.unwrap();
    println!("Part 2 -- Closest Point: {}, {} Distance: {}", pt.x, pt.y, best_distance);
}

fn part_1(wires: &Vec<Vec<Line>>) {
    let wire1 = &wires[0];
    let wire2 = &wires[1];

    let mut best_point = None;
    let mut best_distance = 9999999999;
    const BASE: Point = Point{x: 0, y: 0};
    for w1 in wire1 {
        for w2 in wire2 {
            if w1.does_intersect(&w2) {
                if let Some(pt) = w1.get_intersection(&w2) {
                    if pt != BASE {
                        let distance = pt.manhattan_distance(&BASE);
                        if distance < best_distance {
                            best_point = Some(pt);
                            best_distance = distance;
                        }
                    }
                }
            }
        }
    }

    let pt = best_point.unwrap();

    println!("Part 1 -- Closest Point: {}, {} Distance: {}", pt.x, pt.y, best_distance);
}

fn main() {
    let now = SystemTime::now();

    let values = fs::read_to_string("./input.txt")
        .expect("failed to read file");

    let wires = parse_wires(&values);
    
    part_1(&wires);
    part_2(&wires);

    if let Ok(elapsed) = now.elapsed() {
        println!();
        println!("Run Time: {}ms", elapsed.as_millis());
    }
}
