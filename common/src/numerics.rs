pub fn decompose_usize(num: usize) -> Vec<usize> {
    let mut n = num;
    let mut ret = Vec::new();

    while n > 9 {
        ret.insert(0, n % 10);
        n /= 10;
    }

    ret.insert(0, n);
    ret
}

pub fn decompose_i64(num: i64) -> Vec<usize> {
    let num = num.abs() as usize;
    decompose_usize(num)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn can_decomponse_usize() {
        let n = 12345678910;

        let result = decompose_usize(n);

        assert_eq!(result, vec![1,2,3,4,5,6,7,8,9,1,0]);
    }
}