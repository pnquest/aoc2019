use std::boxed::Box;
use super::*; 

pub struct Exit {

}

impl Exit {
    pub fn from_program(position: usize, program: &Vec<i64>) -> Result<Box<dyn Executable>, String> {
        const CODE: usize = 99;

        if program[position] as usize != CODE {
            return Err("This is not an exit instruction".to_string());
        }

        Ok(Box::new(Exit{}))
    }
}

impl Executable for Exit {
    fn execute(&self, _: &mut Vec<i64>, _: &mut i64, 
        _receiver: &Receiver<i64>, _sender: &Sender<Option<i64>>) -> FinalAction {
        
            FinalAction::Exit
    }
}