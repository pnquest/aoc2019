use super::*;
use std::sync::mpsc::{Receiver};

pub struct Reader {
    address: Parameter
}

impl Reader {
    pub fn from_program(position: usize, program: &mut Vec<i64>, param_modes: Vec<usize>) 
        -> Result<Box<dyn Executable>, String> {
        
        const CODE: usize = 3;
        
        if  extract_raw_instruction(program[position]) != CODE {
            return Err("This is not a write instruction".to_string());
        }

        if !check_remaining_space(position, 1, program) {
            return Err("Not enough instructions remaining".to_string());
        }

        let mut modes = param_modes;

        pad_modes(&mut modes, 1);

        let address = Parameter::create(modes[0], program[position + 1]);
        Ok(Box::new(Reader{address: address}))
    }
}

impl Executable for Reader {
    fn execute(&self, program: &mut Vec<i64>, relative_base: &mut i64, receiver: &Receiver<i64>, 
        _sender: &Sender<Option<i64>>) -> FinalAction {
        
        if let Ok(v) = receiver.recv() {
            let addr = self.address.extract_address(program, *relative_base);
            program[addr] = v;
            return FinalAction::Increment(2);
        }
        FinalAction::Exit
    }
}