extern crate common;

use common::geometry::Point;
use common::int_computer::Computer;
use common::pathfinding::*;

use std::cmp::{max, min};
use std::collections::BTreeMap;
use std::sync::mpsc::channel;
use std::thread::spawn;
use std::time::SystemTime;

#[derive(Clone, Copy)]
enum SpaceType {
    Open = 1,
    Wall = 0,
    O2 = 2,
    Unknown = 3,
    Drone = 4,
}

impl SpaceType {
    fn from_i64(value: i64) -> SpaceType {
        match value {
            1 => SpaceType::Open,
            0 => SpaceType::Wall,
            2 => SpaceType::O2,
            3 => SpaceType::Unknown,
            _ => panic!("Unsupported value"),
        }
    }

    fn is_unknown(&self) -> bool {
        if let SpaceType::Unknown = self {
            return true;
        }

        false
    }
    fn is_o2(&self) -> bool {
        if let SpaceType::O2 = self {
            return true;
        }

        false
    }
    fn is_open(&self) -> bool {
        if let SpaceType::Open = self {
            return true;
        }

        false
    }
}

#[derive(Clone, Copy)]
enum Direction {
    North = 1,
    South = 2,
    West = 3,
    East = 4,
}

impl Direction {
    fn to_i64(&self) -> i64 {
        let num = *self as i64;
        num
    }

    fn get_opposite(&self) -> Self {
        match self {
            Direction::North => Direction::South,
            Direction::South => Direction::North,
            Direction::East => Direction::West,
            Direction::West => Direction::East,
        }
    }

    fn get_move_point(&self) -> Point {
        match self {
            Direction::North => Point { x: 0, y: 1 },
            Direction::South => Point { x: 0, y: -1 },
            Direction::East => Point { x: 1, y: 0 },
            Direction::West => Point { x: -1, y: 0 },
        }
    }
}

fn main() {
    let now = SystemTime::now();
    let (mut map, o2_start) = part_1();
    part_2(&mut map, o2_start);
    if let Ok(elapsed) = now.elapsed() {
        println!();
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn part_2(map: &mut BTreeMap<Point, SpaceType>, o2_start: Point) {
    let mut minutes = 0;
    let open_count = map.iter().filter(|(_, v)| v.is_open()).count();
    let mut o2_points = vec![o2_start];
    let mut filled_count = 0;

    loop {
        minutes += 1;
        for p in o2_points.clone() {
            let mut neighbors = Vec::new();
            neighbors.push((p.translate(0, 1), *map.entry(p.translate(0, 1)).or_insert(SpaceType::Wall)));
            neighbors.push((p.translate(0, -1),*map.entry(p.translate(0, -1)).or_insert(SpaceType::Wall)));
            neighbors.push((p.translate(-1, 0),*map.entry(p.translate(-1, 0)).or_insert(SpaceType::Wall)));
            neighbors.push((p.translate(1, 0),*map.entry(p.translate(1, 0)).or_insert(SpaceType::Wall)));

            for (pn,n) in neighbors {
                if n.is_open() {
                    let ent = map.entry(pn).or_insert(SpaceType::O2);
                    *ent = SpaceType::O2;
                    o2_points.push(pn);
                    filled_count += 1;
                }
            }
        }
        if filled_count == open_count {
            println!("Part 2: {} minutes", minutes);
            return;
        }
    }
}

fn part_1() -> (BTreeMap<Point, SpaceType>, Point) {
    let program = vec![
        3, 1033, 1008, 1033, 1, 1032, 1005, 1032, 31, 1008, 1033, 2, 1032, 1005, 1032, 58, 1008,
        1033, 3, 1032, 1005, 1032, 81, 1008, 1033, 4, 1032, 1005, 1032, 104, 99, 1002, 1034, 1,
        1039, 102, 1, 1036, 1041, 1001, 1035, -1, 1040, 1008, 1038, 0, 1043, 102, -1, 1043, 1032,
        1, 1037, 1032, 1042, 1105, 1, 124, 1002, 1034, 1, 1039, 101, 0, 1036, 1041, 1001, 1035, 1,
        1040, 1008, 1038, 0, 1043, 1, 1037, 1038, 1042, 1106, 0, 124, 1001, 1034, -1, 1039, 1008,
        1036, 0, 1041, 101, 0, 1035, 1040, 1001, 1038, 0, 1043, 1001, 1037, 0, 1042, 1105, 1, 124,
        1001, 1034, 1, 1039, 1008, 1036, 0, 1041, 1002, 1035, 1, 1040, 102, 1, 1038, 1043, 101, 0,
        1037, 1042, 1006, 1039, 217, 1006, 1040, 217, 1008, 1039, 40, 1032, 1005, 1032, 217, 1008,
        1040, 40, 1032, 1005, 1032, 217, 1008, 1039, 37, 1032, 1006, 1032, 165, 1008, 1040, 37,
        1032, 1006, 1032, 165, 1102, 1, 2, 1044, 1106, 0, 224, 2, 1041, 1043, 1032, 1006, 1032,
        179, 1101, 1, 0, 1044, 1105, 1, 224, 1, 1041, 1043, 1032, 1006, 1032, 217, 1, 1042, 1043,
        1032, 1001, 1032, -1, 1032, 1002, 1032, 39, 1032, 1, 1032, 1039, 1032, 101, -1, 1032, 1032,
        101, 252, 1032, 211, 1007, 0, 73, 1044, 1105, 1, 224, 1102, 1, 0, 1044, 1105, 1, 224, 1006,
        1044, 247, 1002, 1039, 1, 1034, 1001, 1040, 0, 1035, 101, 0, 1041, 1036, 101, 0, 1043,
        1038, 101, 0, 1042, 1037, 4, 1044, 1105, 1, 0, 58, 87, 52, 69, 28, 16, 88, 43, 75, 16, 91,
        2, 94, 51, 62, 80, 96, 46, 64, 98, 72, 8, 54, 71, 47, 84, 88, 44, 81, 7, 90, 13, 80, 42,
        62, 68, 85, 27, 34, 2, 13, 89, 87, 79, 63, 76, 9, 82, 58, 60, 93, 63, 78, 79, 43, 32, 84,
        25, 34, 80, 87, 15, 89, 96, 1, 50, 75, 25, 67, 82, 27, 3, 89, 48, 99, 33, 36, 77, 86, 62,
        99, 19, 86, 92, 6, 56, 24, 96, 2, 79, 9, 3, 84, 41, 94, 79, 76, 91, 66, 50, 82, 88, 85, 13,
        88, 18, 93, 79, 12, 98, 46, 75, 52, 99, 95, 11, 16, 25, 17, 77, 55, 87, 17, 74, 76, 81, 41,
        77, 80, 92, 46, 20, 99, 22, 16, 41, 90, 64, 89, 53, 3, 61, 88, 97, 14, 2, 33, 79, 62, 79,
        90, 80, 77, 71, 45, 40, 51, 62, 67, 82, 42, 27, 97, 17, 72, 77, 12, 38, 97, 85, 85, 35, 92,
        82, 3, 84, 96, 40, 27, 93, 96, 18, 45, 98, 16, 49, 82, 52, 90, 43, 81, 10, 88, 94, 15, 42,
        77, 67, 84, 88, 51, 35, 84, 20, 99, 7, 9, 79, 65, 86, 39, 93, 52, 98, 11, 19, 83, 75, 92,
        27, 72, 77, 77, 78, 99, 18, 53, 35, 75, 14, 23, 90, 15, 83, 15, 98, 74, 14, 75, 67, 98, 93,
        64, 97, 97, 58, 77, 88, 28, 19, 1, 82, 96, 69, 92, 34, 1, 90, 45, 79, 27, 25, 85, 59, 89,
        88, 13, 91, 93, 38, 95, 55, 24, 61, 79, 56, 63, 61, 80, 10, 76, 84, 24, 80, 41, 83, 37, 86,
        81, 93, 53, 33, 75, 78, 6, 81, 66, 84, 98, 3, 37, 84, 48, 89, 88, 70, 93, 96, 17, 94, 38,
        82, 39, 74, 65, 90, 9, 77, 55, 53, 78, 10, 98, 27, 96, 11, 18, 86, 54, 98, 53, 86, 66, 19,
        93, 52, 99, 44, 85, 79, 19, 7, 53, 86, 13, 90, 46, 33, 86, 19, 52, 79, 60, 92, 94, 97, 4,
        99, 83, 67, 84, 58, 10, 96, 5, 91, 75, 47, 74, 93, 68, 76, 74, 50, 45, 99, 15, 85, 13, 99,
        96, 30, 99, 84, 59, 81, 51, 64, 74, 9, 27, 2, 99, 34, 49, 76, 61, 28, 87, 56, 84, 81, 32,
        6, 88, 48, 57, 89, 43, 76, 77, 15, 80, 91, 45, 9, 6, 52, 93, 84, 77, 17, 82, 32, 67, 97,
        92, 74, 54, 46, 99, 80, 5, 83, 74, 85, 64, 89, 36, 41, 77, 47, 94, 24, 86, 45, 23, 99, 59,
        90, 43, 61, 95, 98, 91, 90, 33, 91, 15, 19, 88, 49, 54, 86, 75, 42, 67, 43, 54, 97, 10, 10,
        42, 85, 10, 11, 60, 76, 17, 90, 43, 80, 80, 34, 90, 85, 71, 70, 40, 80, 97, 31, 55, 80, 3,
        58, 99, 31, 31, 99, 31, 90, 90, 57, 29, 85, 76, 22, 14, 77, 76, 87, 21, 88, 77, 85, 33, 81,
        77, 94, 57, 56, 18, 83, 54, 90, 90, 2, 89, 87, 36, 13, 85, 36, 85, 70, 96, 20, 85, 82, 43,
        34, 97, 93, 27, 40, 44, 80, 97, 2, 81, 16, 44, 12, 91, 35, 90, 24, 49, 75, 71, 96, 5, 29,
        65, 80, 87, 35, 51, 92, 43, 94, 30, 84, 88, 10, 99, 4, 71, 76, 65, 77, 71, 1, 89, 90, 58,
        28, 77, 42, 57, 81, 87, 13, 16, 72, 74, 32, 98, 83, 8, 75, 79, 10, 96, 11, 92, 34, 84, 13,
        1, 77, 78, 71, 21, 63, 78, 37, 98, 86, 53, 84, 75, 1, 60, 75, 66, 86, 22, 78, 32, 31, 78,
        97, 97, 89, 23, 88, 78, 4, 75, 59, 99, 65, 13, 85, 70, 74, 77, 83, 39, 62, 76, 81, 33, 98,
        87, 25, 41, 90, 48, 42, 33, 24, 94, 86, 15, 94, 89, 21, 23, 81, 29, 36, 99, 93, 60, 20, 90,
        19, 66, 52, 90, 80, 97, 95, 21, 86, 45, 80, 78, 7, 37, 80, 84, 22, 6, 97, 79, 34, 87, 27,
        43, 52, 97, 84, 72, 9, 89, 93, 2, 75, 82, 60, 92, 12, 87, 89, 59, 74, 64, 90, 38, 71, 89,
        12, 26, 81, 6, 53, 78, 96, 8, 81, 91, 69, 68, 89, 76, 79, 50, 77, 19, 83, 14, 75, 26, 76,
        34, 78, 1, 83, 70, 80, 39, 99, 62, 95, 89, 99, 6, 79, 93, 80, 10, 83, 50, 79, 80, 92, 41,
        78, 20, 86, 9, 84, 53, 87, 13, 74, 0, 0, 21, 21, 1, 10, 1, 0, 0, 0, 0, 0, 0,
    ];

    let (in_send, in_rec) = channel();
    let (out_send, out_rec) = channel();

    let mut comp = Computer::from_vec_with_providers(program, in_rec, &out_send);
    spawn(move || comp.run_computer());
    let mut cur_position = Point { x: 0, y: 0 };
    let mut steps = Vec::new();
    let mut map = BTreeMap::new();
    map.insert(cur_position, SpaceType::Open);
    let mut o2_position = Point { x: 0, y: 0 };
    loop {
        let move_info = select_new_direction(cur_position, &mut steps, &mut map);
        match move_info {
            None => break,
            Some((dir, should_log)) => {
                in_send.send(dir.to_i64()).expect("failed to send");
                if let Some(v) = out_rec.recv().expect("could not receive") {
                    let typ = SpaceType::from_i64(v);
                    let lg = match typ {
                        SpaceType::Open => {
                            let mv = dir.get_move_point();
                            cur_position = cur_position.translate(mv.x, mv.y);
                            let ent = map.entry(cur_position).or_insert(SpaceType::Unknown);
                            *ent = SpaceType::Open;
                            should_log
                        }
                        SpaceType::O2 => {
                            let mv = dir.get_move_point();
                            cur_position = cur_position.translate(mv.x, mv.y);
                            o2_position = cur_position;
                            let ent = map.entry(cur_position).or_insert(SpaceType::Unknown);
                            *ent = SpaceType::O2;
                            should_log
                        }
                        SpaceType::Wall => {
                            let mv = dir.get_move_point();
                            let ent = map
                                .entry(cur_position.translate(mv.x, mv.y))
                                .or_insert(SpaceType::Unknown);
                            *ent = SpaceType::Wall;
                            false
                        }
                        _ => panic!("The thing that should not be!"),
                    };
                    if lg {
                        steps.push(dir);
                    }
                }
            }
        }
    }
    print_map(cur_position, map.clone());
    println!();
    println!("O2 is at: {:?}", o2_position);
    let mp = convert_map(&map);
    let res = solve_map(&mp, Point { x: 0, y: 0 }, o2_position).unwrap();
    println!("Part 1: {}", res.len() - 1);
    //needed to avoid panic when the computer shuts down
    drop(in_send);
    (map, o2_position)
}

fn convert_map(map: &BTreeMap<Point, SpaceType>) -> Map {
    let mut ret = BTreeMap::new();

    for (p, v) in map {
        if p.x == 0 && p.y == 0 {
            ret.insert(*p, Space::Start(*p));
        }

        let sp = match v {
            SpaceType::Open => Space::Open(*p),
            SpaceType::Unknown => Space::Blocked(*p),
            SpaceType::Wall => Space::Blocked(*p),
            SpaceType::O2 => Space::Goal(*p),
            _ => panic!("bad!"),
        };

        ret.insert(*p, sp);
    }

    ret
}

fn print_map(drone_position: Point, map: BTreeMap<Point, SpaceType>) {
    println!();
    println!("-----");
    let mut mp = map;
    let ent = mp.entry(drone_position).or_insert(SpaceType::Unknown);
    if !ent.is_o2() {
        *ent = SpaceType::Drone;
    }
    let (x_x, m_x, x_y, m_y) = mp.iter().fold(
        (
            std::isize::MIN,
            std::isize::MAX,
            std::isize::MIN,
            std::isize::MAX,
        ),
        |(max_x, min_x, max_y, min_y), (p, _)| {
            (
                max(max_x, p.x),
                min(min_x, p.x),
                max(max_y, p.y),
                min(min_y, p.y),
            )
        },
    );

    for y in (m_y..=x_y).rev() {
        for x in m_x..=x_x {
            let ent = mp.entry(Point { x: x, y: y }).or_insert(SpaceType::Unknown);
            if x == 0 && y == 0 {
                print!("*");
            } else {
                match ent {
                    SpaceType::Drone => print!("D"),
                    SpaceType::Open => print!("."),
                    SpaceType::Wall => print!("#"),
                    SpaceType::O2 => print!("O"),
                    SpaceType::Unknown => print!("?"),
                };
            }
        }
        println!();
    }
}

fn select_new_direction(
    cur_position: Point,
    steps: &mut Vec<Direction>,
    map: &mut BTreeMap<Point, SpaceType>,
) -> Option<(Direction, bool)> {
    let space_above = *map
        .entry(cur_position.translate(0, 1))
        .or_insert(SpaceType::Unknown);
    let space_below = *map
        .entry(cur_position.translate(0, -1))
        .or_insert(SpaceType::Unknown);
    let space_west = *map
        .entry(cur_position.translate(-1, 0))
        .or_insert(SpaceType::Unknown);
    let space_east = *map
        .entry(cur_position.translate(1, 0))
        .or_insert(SpaceType::Unknown);

    if space_east.is_unknown() {
        return Some((Direction::East, true));
    }
    if space_west.is_unknown() {
        return Some((Direction::West, true));
    }
    if space_above.is_unknown() {
        return Some((Direction::North, true));
    }
    if space_below.is_unknown() {
        return Some((Direction::South, true));
    }

    if steps.len() == 0 {
        return None;
    }
    let prev = steps.remove(steps.len() - 1);

    Some((prev.get_opposite(), false))
}
