use super::*;
use std::boxed::Box;

pub struct EqualTo {
    param_1: Parameter,
    param_2: Parameter,
    target: Parameter
}

impl EqualTo {
    pub fn from_program(position: usize, program: &mut Vec<i64>, param_modes: Vec<usize>) 
        -> Result<Box<dyn Executable>, String> {
        
        const CODE: usize = 8;

        let raw_instruction = extract_raw_instruction(program[position]);
        
        if raw_instruction != CODE {
            return Err("Not an equal to instruction".to_string());
        }

        if !check_remaining_space(position, 3, program) {
            return Err("Not enough instructions remaining".to_string());
        }

        let mut modes = param_modes;

        pad_modes(&mut modes, 3);

        Ok(Box::new(EqualTo {
            param_1: Parameter::create(modes[2], program[position + 1]),
            param_2: Parameter::create(modes[1], program[position + 2]),
            target: Parameter::create(modes[0], program[position + 3])
        }))
    }
}

impl Executable for EqualTo {
    fn execute(&self, program: &mut Vec<i64>, relative_base: &mut i64
        , _receiver: &Receiver<i64>, _sender: &Sender<Option<i64>>) -> FinalAction {
        let p1 = self.param_1.extract_value(program, *relative_base);
        let p2 = self.param_2.extract_value(program, *relative_base);

        let assignment = (p1 == p2) as i64;

        let target = self.target.extract_address(program, *relative_base);

        ensure_enough_space(target as usize, program);
        program[target as usize] = assignment;

        FinalAction::Increment(4)
    }
}