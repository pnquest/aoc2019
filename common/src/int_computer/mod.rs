mod instructions;
use instructions::*;

use std::sync::mpsc::{Receiver, Sender};

pub struct Computer {
    instructions: Vec<i64>,
    orig_instructions: Vec<i64>,
    position: usize,
    input_provider: Receiver<i64>,
    output_provider: Sender<Option<i64>>,
    relative_base: i64
}

impl Computer {

    pub fn from_vec_with_providers(instructions: Vec<i64>, 
        input_provider: Receiver<i64>, 
        output_provider: &Sender<Option<i64>>) -> Computer {
            Computer {
                instructions: instructions.clone(),
                orig_instructions: instructions,
                position: 0,
                input_provider: input_provider,
                output_provider: output_provider.clone(),
                relative_base: 0
            }
    }

    pub fn reset(&mut self) {
        self.position = 0;
        self.relative_base = 0;
        self.instructions = self.orig_instructions.clone();
    }

    pub fn run_computer(&mut self) -> i64 {
        loop {
            let inst = get_instruction(self.position, &mut self.instructions)
                .expect("Error parsing instruction");

            let action = inst.execute(&mut self.instructions, &mut self.relative_base, &self.input_provider, &self.output_provider);

            match action {
                FinalAction::Increment(size) => self.position += size,
                FinalAction::Jump(target) => self.position = target,
                FinalAction::Exit => {
                    self.output_provider.send(None).expect("could not send message");
                    return self.instructions[0];
                }
            };
        }
    }
}