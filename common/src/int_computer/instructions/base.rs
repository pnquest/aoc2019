use super::*;
use std::boxed::Box;

pub struct AdjustBase {
    adjustment: Parameter
}

impl AdjustBase {
    pub fn from_program(position: usize, program: &mut Vec<i64>, param_modes: Vec<usize>) -> Result<Box<dyn Executable>, String> {
        const CODE: usize = 9;
        if extract_raw_instruction(program[position]) != CODE {
            return Err("This is not an adjust base instruction".to_string());
        }

        if !check_remaining_space(position, 1, program) {
            return Err("Not enough instructions remaining".to_string());
        }

        let mut modes = param_modes;

        pad_modes(&mut modes, 1);

        Ok(Box::new(AdjustBase{
            adjustment: Parameter::create(modes[0], program[position + 1])
        }))
    }
}

impl Executable for AdjustBase {
    fn execute(&self, program: &mut Vec<i64>, relative_base: &mut i64
        , _receiver: &Receiver<i64>, _sender: &Sender<Option<i64>>) -> FinalAction {

        let adjustment = self.adjustment.extract_value(program, *relative_base);
        *relative_base += adjustment;

        FinalAction::Increment(2)
    }
}