use super::*;
use std::sync::mpsc::{Sender, Receiver};

pub struct Write {
    address: Parameter
}

impl Write {
    pub fn from_program(position: usize, program: &mut Vec<i64>, param_modes: Vec<usize>) 
        -> Result<Box<dyn Executable>, String> {
        
        const CODE: usize = 4;
        
        if extract_raw_instruction(program[position]) != CODE {
            return Err("This is not a write instruction".to_string());
        }

        if !check_remaining_space(position, 1, program) {
            return Err("Not enough instructions remaining".to_string());
        }

        let mut modes = param_modes;

        pad_modes(&mut modes, 1);

        let address = Parameter::create(modes[0], program[position + 1]);
        Ok(Box::new(Write{address: address}))
    }
}

impl Executable for Write {
    fn execute(&self, program: &mut Vec<i64>, relative_base: &mut i64, 
        _receiver: &Receiver<i64>, sender: &Sender<Option<i64>>) -> FinalAction {
        
        let value = self.address.extract_value(program, *relative_base);
        sender.send(Some(value)).unwrap();
        FinalAction::Increment(2)
    }
}