extern crate common;

use common::numerics::decompose_usize;
use std::time::SystemTime;

fn main() {
    const LB: usize = 125730;
    const UB: usize = 579381;
    let now = SystemTime::now();
    part_1(LB, UB);
    part_2(LB, UB);

    if let Ok(elapsed) = now.elapsed() {
        println!("Elapsed: {}ms", elapsed.as_millis());
    } 
}

fn part_2(lower: usize, upper: usize) {
    let mut count = 0;

    for v in lower..=upper {
        let digits = decompose_usize(v);
        let mut doesnt_decrease = true;
        let mut min_duplicate_sequence = 99999;
        let mut cur_duplicate_sequence = 0;

        let mut prev_digit = Option::None;
        for d in digits {
            if let Option::Some(p) = prev_digit {
                if p == d {
                    cur_duplicate_sequence += 1;
                } 
                else if cur_duplicate_sequence != 0 
                    && cur_duplicate_sequence < min_duplicate_sequence {

                    min_duplicate_sequence = cur_duplicate_sequence;
                    cur_duplicate_sequence = 0;
                }

                if d < p {
                    doesnt_decrease = false;
                }
            }
            prev_digit = Option::Some(d);
        }

        if cur_duplicate_sequence != 0 
            && cur_duplicate_sequence < min_duplicate_sequence {
                
            min_duplicate_sequence = cur_duplicate_sequence;
        }

        if doesnt_decrease && min_duplicate_sequence == 1 {
            count += 1;
        }
    }

    println!("Part 2: The count is {}", count);
}

fn part_1(lower: usize, upper: usize) {
    let mut count = 0;


    for v in lower..=upper {
        let digits = decompose_usize(v);
        let mut found_pair = false;
        let mut doesnt_decrease = true;

        let mut prev_digit = Option::None;

        for d in digits {
            if let Option::Some(p) = prev_digit {
                if p == d {
                    found_pair = true;
                }
                else if d < p {
                    doesnt_decrease = false;
                }
            }
            prev_digit = Option::Some(d);
        }

        if found_pair && doesnt_decrease {
            count += 1;
        }
    }

    println!("Part 1: The count is {}", count);
}