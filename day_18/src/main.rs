extern crate common;

use common::geometry::Point;
use common::pathfinding::*;

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::fs::read_to_string;
use std::time::SystemTime;

#[derive(Copy, Clone)]
enum SpaceType {
    Wall,
    Door(char),
    Key(char),
    Open,
    Start,
}

impl SpaceType {
    fn is_door(&self) -> bool {
        if let SpaceType::Door(_) = self {
            return true;
        }

        false
    }

    fn is_key(&self) -> bool {
        if let SpaceType::Key(_) = self {
            return true;
        }

        false
    }

    fn is_start(&self) -> bool {
        if let SpaceType::Start = self {
            return true;
        }

        false
    }
}

#[derive(Clone)]
struct Path {
    points: Vec<Point>,
    doors: Vec<char>,
    keys: Vec<char>,
    start: Point,
    end: Point,
    ending_key: char,
}

#[derive(Hash, Eq, PartialEq)]
struct CacheKey {
    position1: Point,
    position2: Option<Point>,
    position3: Option<Point>,
    position4: Option<Point>,
    found: String,
}

impl Path {
    fn length(&self) -> usize {
        self.points.len() - 1
    }

    fn is_passable(&self, keys_collected: &HashSet<char>) -> bool {
        let mut passable = true;

        for d in &self.doors {
            if !keys_collected.contains(d) {
                passable = false;
                break;
            }
        }

        passable
    }
}

fn main() {
    let now = SystemTime::now();
    part_1();
    part_2();
    if let Ok(elapsed) = now.elapsed() {
        println!();
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn part_1() {
    if let Some((len, path_string)) = solve("input.txt") {
        println!(
            "Part 1: {} -- {}",
            len,
            &path_string[..path_string.len() - 1]
        );
    }
}

fn part_2() {
    if let Some((len, path_string)) = solve("input2.txt") {
        println!(
            "Part 2: {} -- {}",
            len,
            &path_string[..path_string.len() - 1]
        );
    }
}

fn solve(in_file: &str) -> Option<(usize, String)> {
    let input = read_to_string(in_file).expect("could not open file");

    let mut map = BTreeMap::new();
    let mut keys = BTreeMap::new();
    let mut doors = BTreeMap::new();
    let mut start = Vec::new();

    let mut paths = BTreeMap::new();

    read_map(input, &mut map, &mut keys, &mut doors, &mut start);

    let total_keys = keys.len();

    //pre-compute all possible paths from the start, treating doors as passable
    for st in &start {
        compute_paths(*st, &keys, &map, &doors, &mut paths);
    }

    //pre-compute all possible paths from each key to each other key, treating doors as passable
    for (key, _) in &keys {
        compute_paths(*key, &keys, &map, &doors, &mut paths);
    }
    let cache = RefCell::new(HashMap::new());
    let (len, found, final_path) = find_shortest_path(
        &paths,
        start,
        HashSet::new(),
        Vec::new(),
        total_keys,
        &cache,
    );
    let path_string = final_path.iter().fold("".to_string(), |acc, v| {
        let mut st = acc;
        st.push(*v);
        st.push(',');
        st
    });
    if found {
        Some((len, path_string))
    } else {
        None
    }
}

fn create_cache_key(starts: &Vec<Point>, owned_keys: &HashSet<char>) -> CacheKey {
    let p1 = starts[0];
    let p2 = if starts.len() >= 2 {
        Some(starts[1])
    } else {
        None
    };
    let p3 = if starts.len() >= 3 {
        Some(starts[2])
    } else {
        None
    };
    let p4 = if starts.len() >= 4 {
        Some(starts[3])
    } else {
        None
    };

    CacheKey {
        position1: p1,
        position2: p2,
        position3: p3,
        position4: p4,
        found: get_found_keys(owned_keys),
    }
}

fn find_shortest_path(
    paths: &BTreeMap<Point, Vec<Path>>,
    start: Vec<Point>,
    owned_keys: HashSet<char>,
    ordered_keys: Vec<char>,
    total_keys: usize,
    cache: &RefCell<HashMap<CacheKey, (usize, Vec<char>)>>,
) -> (usize, bool, Vec<char>) {
    let key = create_cache_key(&start, &owned_keys);

    let ca = cache.borrow();
    if let Some((l, p)) = ca.get(&key) {
        return (*l, true, p.clone());
    }
    drop(ca);
    let mut complete_paths = Vec::new();
    for s in 0..start.len() {
        let st = start[s];
        if paths.contains_key(&st) {
            let my_paths = &paths[&st];
            for p in my_paths {
                if p.is_passable(&owned_keys) && !owned_keys.contains(&p.ending_key) {
                    let len = p.length();
                    let mut keys = owned_keys.clone();
                    let mut ord = ordered_keys.clone();
                    ord.push(p.ending_key);
                    for k in &p.keys {
                        keys.insert(*k);
                    }

                    if keys.len() == total_keys {
                        complete_paths.push((len, ord));
                    } else {
                        let mut new_starts = start.clone();
                        new_starts[s] = p.end;
                        let (l, found, ord) =
                            find_shortest_path(paths, new_starts, keys, ord, total_keys, cache);

                        if found {
                            complete_paths.push((len + l, ord));
                        }
                    }
                }
            }
        }
    }
    let shortest = complete_paths.iter().min_by_key(|(l, _)| l);

    if let Some((l, p)) = shortest {
        let mut ca = cache.borrow_mut();
        ca.insert(key, (*l, p.clone()));
        return (*l, true, p.clone());
    }

    (0, false, Vec::new())
}

fn get_found_keys(keys: &HashSet<char>) -> String {
    let mut v: Vec<char> = keys.iter().map(|c| *c).collect();

    v.sort();
    v.iter().fold("".into(), |acc, v| {
        let mut ac = acc;
        ac.push(*v);
        ac
    })
}

fn compute_paths(
    start: Point,
    keys: &BTreeMap<Point, char>,
    map: &BTreeMap<Point, SpaceType>,
    doors: &BTreeMap<Point, char>,
    paths: &mut BTreeMap<Point, Vec<Path>>,
) {
    for (p, _) in keys {
        if *p == start {
            continue;
        }

        let m = translate_map(&map, &start, p);
        if let Some(solution) = solve_map(&m, start, *p) {
            let mut keys_passed = Vec::new();
            let mut doors_passed = Vec::new();

            for st in &solution {
                if let Some(k) = keys.get(st) {
                    keys_passed.push(*k);
                }

                if let Some(d) = doors.get(st) {
                    doors_passed.push(*d);
                }
            }

            let path = Path {
                points: solution,
                doors: doors_passed,
                keys: keys_passed,
                start: start,
                end: *p,
                ending_key: *keys.get(p).unwrap(),
            };
            let ent = paths.entry(start).or_insert(Vec::new());
            ent.push(path);
        }
    }
}

fn translate_map(map: &BTreeMap<Point, SpaceType>, start: &Point, goal: &Point) -> Map {
    let mut ret = BTreeMap::new();

    for (pt, s) in map {
        let p = *pt;
        let mut sp = match s {
            SpaceType::Wall => Space::Blocked(p),
            _ => Space::Open(p),
        };

        if p == *start {
            sp = Space::Start(p);
        } else if p == *goal {
            sp = Space::Goal(p);
        }

        ret.insert(p, sp);
    }

    ret
}

fn read_map(
    input: String,
    map: &mut BTreeMap<Point, SpaceType>,
    keys: &mut BTreeMap<Point, char>,
    doors: &mut BTreeMap<Point, char>,
    start: &mut Vec<Point>,
) {
    let mut y = 0;
    for l in input.lines() {
        let mut x = 0;
        for c in l.chars() {
            let mut space = match c {
                '#' => SpaceType::Wall,
                '.' => SpaceType::Open,
                '@' => SpaceType::Start,
                'A'..='Z' => {
                    let lower = ((c as u8) + 32) as char;
                    SpaceType::Door(lower)
                }
                'a'..='z' => SpaceType::Key(c),
                _ => panic!("Bad space!"),
            };

            let pt = Point { x: x, y: y };

            if space.is_door() {
                doors.insert(pt, ((c as u8) + 32) as char);
            } else if space.is_key() {
                keys.insert(pt, c);
            } else if space.is_start() {
                start.push(pt);
                space = SpaceType::Open;
            }
            map.insert(pt, space);
            x += 1;
        }
        y += 1;
    }
}
