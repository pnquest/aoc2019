extern crate common;

use common::int_computer::*;
use common::geometry::Point;
use std::thread::{spawn};
use std::sync::mpsc::{channel};
use std::collections::{HashSet, HashMap};
use std::time::SystemTime;

struct PainterBot {
    program: Vec<i64>,
    facing: Point,
    position: Point,
    starting_color: Color
}

#[derive(Copy, Clone)]
enum Color {
    Black = 0,
    White = 1
}

enum Turn {
    Left = 0,
    Right = 1
}

impl PainterBot {
    fn run(&mut self) -> (HashSet<Point>, HashMap<Point, Color>) {
        let mut ret = HashSet::new();
        let prg = self.program.clone();
        let (in_send, in_rec) = channel();
        let (out_send, out_rec) = channel();
        let mut cmp = Computer::from_vec_with_providers(prg, in_rec, &out_send);
        let mut positions = HashMap::new();
        positions.insert(Point{x:0, y:0}, self.starting_color);
        let handle = spawn(move || {
            cmp.run_computer()
        });

        loop {
            let ent = positions.entry(self.position).or_insert(Color::Black);
            in_send.send(*ent as i64).expect("could not send data");
            let r1 = out_rec.recv().expect("could not get data");
            if r1.is_none() {
                break;
            }
            let r2 = out_rec.recv().expect("could not get data");

            let color = match r1.unwrap() {
                0 => Color::Black,
                1 => Color::White,
                _ => panic!("Bad color")
            };
            let turn = match r2.unwrap(){
                0 => Turn::Left,
                1 => Turn::Right,
                _ => panic!("bad turn")
            };

            let ent = positions.entry(self.position).or_insert(Color::Black);
            *ent = color;
            ret.insert(self.position);

            match turn {
                Turn::Left => {
                    if self.facing.x == 1 {
                        self.facing = Point{x:0, y:1};
                    } else if self.facing.x == -1 {
                        self.facing = Point{x: 0, y: -1};
                    } else if self.facing.y == 1 {
                        self.facing = Point{x: -1, y: 0};
                    } else {
                        self.facing = Point{x: 1, y:0};
                    }
                },
                Turn::Right => {
                    if self.facing.x == 1 {
                        self.facing = Point{x:0, y:-1};
                    } else if self.facing.x == -1 {
                        self.facing = Point{x: 0, y: 1};
                    } else if self.facing.y == 1 {
                        self.facing = Point{x: 1, y: 0};
                    } else {
                        self.facing = Point{x: -1, y:0};
                    }
                }
            };

            self.position = self.position.translate(self.facing.x, self.facing.y);
        }

        let _f = handle.join().expect("could not join");
        (ret, positions)
    }
}

fn main() {
    let now = SystemTime::now();
    let program = vec![3,8,1005,8,301,1106,0,11,0,0,0,104,1,104,0,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,0,10,4,10,1002,
    8,1,29,1,1103,7,10,3,8,102,-1,8,10,101,1,10,10,4,10,108,1,8,10,4,10,1002,8,1,54,2,103,3,10,2,1008,6,10,1006,0,38,2,
    1108,7,10,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,91,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,0,10,
    4,10,101,0,8,114,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,1,10,4,10,1001,8,0,136,3,8,1002,8,-1,10,1001,10,1,10,4,10,
    1008,8,1,10,4,10,1002,8,1,158,1,1009,0,10,2,1002,18,10,3,8,102,-1,8,10,101,1,10,10,4,10,108,0,8,10,4,10,1002,8,1,187,
    2,1108,6,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,0,8,10,4,10,1002,8,1,213,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,
    1,10,4,10,1001,8,0,236,1,104,10,10,1,1002,20,10,2,1008,9,10,3,8,102,-1,8,10,101,1,10,10,4,10,108,0,8,10,4,10,101,0,8,
    269,1,102,15,10,1006,0,55,2,1107,15,10,101,1,9,9,1007,9,979,10,1005,10,15,99,109,623,104,0,104,1,21102,1,
    932700598932,1,21102,318,1,0,1105,1,422,21102,1,937150489384,1,21102,329,1,0,1105,1,422,3,10,104,0,104,1,3,10,104,0,
    104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21101,46325083227,0,1,21102,376,1,0,1106,0,
    422,21102,3263269927,1,1,21101,387,0,0,1105,1,422,3,10,104,0,104,0,3,10,104,0,104,0,21102,988225102184,1,1,21101,410,0,
    0,1105,1,422,21101,868410356500,0,1,21102,1,421,0,1106,0,422,99,109,2,21202,-1,1,1,21102,1,40,2,21102,1,453,3,21102,1,
    443,0,1105,1,486,109,-2,2106,0,0,0,1,0,0,1,109,2,3,10,204,-1,1001,448,449,464,4,0,1001,448,1,448,108,4,448,10,1006,10,
    480,1102,1,0,448,109,-2,2106,0,0,0,109,4,1201,-1,0,485,1207,-3,0,10,1006,10,503,21101,0,0,-3,22101,0,-3,1,21201,-2,0,2,
    21102,1,1,3,21101,0,522,0,1105,1,527,109,-4,2106,0,0,109,5,1207,-3,1,10,1006,10,550,2207,-4,-2,10,1006,10,550,22102,1,
    -4,-4,1105,1,618,21201,-4,0,1,21201,-3,-1,2,21202,-2,2,3,21102,569,1,0,1106,0,527,22101,0,1,-4,21101,0,1,-1,2207,-4,-2,
    10,1006,10,588,21102,1,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,610,21201,-1,0,1,21101,610,0,0,105,1,485,21202,-2,-1,
    -2,22201,-4,-2,-4,109,-5,2105,1,0];

    part_1(program.clone());
    part_2(program);

    if let Ok(elapsed) = now.elapsed() {
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn part_1(program: Vec<i64>) {
    let mut bot = PainterBot{
        program: program, 
        facing: Point{x:0, y:1}, 
        position: Point{x:0,y:0}, 
        starting_color: Color::Black
    };
    let (result, _) = bot.run();
    println!("Part 1: {}", result.len());
}

fn part_2(program: Vec<i64>) {
    let mut bot = PainterBot{
        program: program, 
        facing: Point{x:0, y:1}, 
        position: Point{x:0,y:0}, 
        starting_color: Color::White
    };
    let (result, mut grid) = bot.run();
    let mut min_x = std::isize::MAX;
    let mut max_x = std::isize::MIN;
    let mut min_y = std::isize::MAX;
    let mut max_y = std::isize::MIN;

    for r in result {
        if r.x < min_x {
            min_x = r.x;
        }
        if r.x > max_x {
            max_x = r.x;
        }
        if r.y < min_y {
            min_y = r.y;
        }
        if r.y > max_y {
            max_y = r.y;
        }
    }
    min_x -= 2;
    max_x += 2;
    min_y -= 2;
    max_y += 2;
    println!("Part 2:");

    for y in (min_y..=max_y).rev() {
        for x in min_x..=max_x {
            let ent = grid.entry(Point{x:x, y:y}).or_insert(Color::Black);

            match ent {
                Color::Black => print!(" "),
                Color::White => print!("#")
            };
        }
        println!();
    }
}
