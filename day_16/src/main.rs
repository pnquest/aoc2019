extern crate common;

use common::iter::ExpansionIterator;
use std::fs::read_to_string;
use std::time::SystemTime;

fn main() {
    let now = SystemTime::now();
    let input = read_to_string("./input.txt").expect("could not read file");
    let digits: Vec<usize> = input
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect();
    part_1(digits.clone());
    part_2(digits);

    if let Ok(elapsed) = now.elapsed() {
        println!();
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn part_2(digits: Vec<usize>) {
    let single = digits.clone();

    let offset = single
        .iter()
        .take(7)
        .rev()
        .enumerate()
        .fold(0, |total, (i, v)| total + 10usize.pow(i as u32) * v);

    let mut digits = Vec::with_capacity(10000 * single.len());

    for _ in 0..10000 {
        digits.append(&mut single.clone());
    }

    digits = digits[offset..].iter().map(|d| *d).collect();

    for _ in 0..100 {
        digits = run_round_2(digits);
    }

    print!("Part 2: ");
    for d in digits.iter().take(8) {
        print!("{}", d);
    }
    println!();
}

fn part_1(digits: Vec<usize>) {
    let mut digits = digits;
    for _ in 0..100 {
        digits = run_round(digits);
    }

    print!("Part 1: ");
    for i in 0..8 {
        print!("{}", digits[i]);
    }
    println!();
}

fn run_round_2(input: Vec<usize>) -> Vec<usize> {
    let total = input.iter().sum::<usize>();
    let first = total % 10;
    let mut transformed: Vec<usize> = input
        .iter()
        .scan(total, |acc, v| {
            *acc -= v;
            if *acc > 0 {
                Some(*acc % 10)
            } else {
                None
            }
        })
        .collect();
    transformed.insert(0, first);

    transformed
}

fn run_round(input: Vec<usize>) -> Vec<usize> {
    (1..=input.len())
        .map(|m| {
            let mask = vec![0, 1, 0, -1];
            let expanded = ExpansionIterator::expand(m, Box::new(mask.iter()));
            (input
                .iter()
                .zip(expanded.cycle().skip(1))
                .map(|(a, b)| *a as isize * b)
                .sum::<isize>()
                .abs()
                % 10) as usize
        })
        .collect()
}
