use std::fs;
use std::time::SystemTime;

fn main() {
    let now = SystemTime::now();
    let file = fs::read_to_string("./input.txt").expect("Could not read file");

    part_1(&file);
    part_2(&file);

    if let Ok(elapsed) = now.elapsed() {
        println!("Run time: {}ms", elapsed.as_millis());
    }
}

fn part_1(file: &String) {
    let result: isize = file.lines()
        .map(|l| l.parse::<isize>().unwrap())
        .map(|m| m / 3 - 2)
        .sum();

    println!("part 1: {}", result);
}

fn part_2(file: &String) {
    let result: isize = file.lines()
        .map(|l| l.parse::<isize>().unwrap())
        .map(|m| calculate_part2_fuel(m))
        .sum();

    println!("part 2: {}", result);
}

fn calculate_part2_fuel(mass: isize) -> isize {
    let needed = mass / 3 - 2;
    if needed > 0 {
        return needed + calculate_part2_fuel(needed);
    }

    return 0;
}
