mod adder;
mod multiplier;
mod exit;
mod write;
mod read;
mod jmp;
mod lt;
mod et;
mod base;

use adder::*;
use multiplier::*;
use exit::*;
use write::*;
use read::*;
use jmp::*;
use lt::*;
use et::*;
use base::*;

use super::super::numerics::{decompose_i64};
use std::sync::mpsc::{Sender, Receiver};

pub enum FinalAction {
    Increment(usize),
    Jump(usize),
    Exit,
}

pub enum Parameter {
    Position(usize),
    Immediate(i64),
    Relative(i64)
}

impl Parameter {
    pub fn create(param_mode: usize, value: i64) -> Parameter {
        match param_mode {
            0 => Parameter::Position(value as usize),
            1 => Parameter::Immediate(value),
            2 => Parameter::Relative(value),
            _ => panic!("unsupported parameter mode!")
        }
    }

    pub fn extract_value(&self, program: &mut Vec<i64>, relative_base: i64) -> i64 {
        match self {
            Parameter::Position(v) => {
                let val = *v;
                ensure_enough_space(val, program);
                program[val]
            },
            Parameter::Immediate(v) => *v as i64,
            Parameter::Relative(v) => {
                let val = (*v + relative_base) as usize;
                ensure_enough_space(val , program);
                program[val]
            }
        }
    }

    pub fn extract_address(&self, program: &mut Vec<i64>, relative_base: i64) -> usize {
        match self {
            Parameter::Position(v) => {
                let val = *v;
                ensure_enough_space(val, program);
                val
            },
            Parameter::Relative(v) => {
                let val = (*v + relative_base) as usize;
                ensure_enough_space(val, program);
                val
            },
            _ => panic!("This is not an address")
        }
    }
}

pub fn pad_modes(modes: &mut Vec<usize>, len: usize) {
    while modes.len() < len {
        modes.insert(0, 0);
    }
}

pub fn check_remaining_space(position: usize, arg_count: usize, program: &mut Vec<i64>) -> bool{
    if position + arg_count > program.len() - 1 {
        program.resize(position + arg_count + 1, 0);
    }
    true
}

pub fn ensure_enough_space(address: usize, program: &mut Vec<i64>) {
    if address > program.len() - 1 {
        program.resize(address + 1, 0);
    }
}

pub trait Executable {
    fn execute(&self, program: &mut Vec<i64>, relative_base: &mut i64, receiver: &Receiver<i64>, sender: &Sender<Option<i64>>) -> FinalAction;
}

pub fn extract_raw_instruction(value: i64) -> usize {
    if value <= 99 {
        return value as usize;
    }

    let decomped = decompose_i64(value);
    let start = decomped.len() - 2;
    let slc = &decomped[start..decomped.len()];
    slc[0] * 10 + slc[1]
}

pub fn get_instruction(position: usize, program: &mut Vec<i64>) -> Result<Box<dyn Executable>, String> {
    let instr = program[position];
    
    if instr < 1 {
        panic!("instruction must be at least 1");
    }

    let code;
    let param_modes;

    if instr > 99 {
        let decomped = decompose_i64(instr);
        let start = decomped.len() - 2;
        let ins_code = &decomped[start..(start + 2)];
        code = ins_code[0] * 10 + ins_code[1];
        param_modes = Vec::from(&decomped[..(decomped.len() - 2)]);
    } else {
        code = instr as usize;
        param_modes = Vec::new();
    }

    match code{
        1 => Adder::from_program(position, program, param_modes),
        2 => Multiplier::from_program(position, program, param_modes),
        3 => Reader::from_program(position, program, param_modes),
        4 => Write::from_program(position, program, param_modes),
        5 => Jump::from_program(position, program, param_modes),
        6 => Jump::from_program(position, program, param_modes),
        7 => LessThan::from_program(position, program, param_modes),
        8 => EqualTo::from_program(position, program, param_modes),
        9 => AdjustBase::from_program(position, program, param_modes), 
        99 => Exit::from_program(position, program),
        _ => Err("Unknown instruction".to_string())
    }
}