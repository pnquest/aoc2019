extern crate indextree;

use indextree::{Arena, NodeId};
use std::collections::HashMap;
use std::fs;
use std::time::SystemTime;

fn main() {
    let now = SystemTime::now();
    let file = fs::read_to_string("./input.txt")
        .expect("Could not read file");

    let (hash, arena) = build_tree(&file);

    part_1(&hash, &arena);
    part_2(&hash, &arena);
    if let Ok(elapsed) = now.elapsed() {
        println!("Elapsed: {}ms", elapsed.as_millis());
    }
}

fn build_tree<'a>(file: &'a str) -> (HashMap<&'a str, NodeId>, Arena<&'a str>) {
    let mut arena = Arena::new();
    let mut hash: HashMap<&str, NodeId> = HashMap::new();

    for line in file.lines() {
        let first = &line[..3];
        let second = &line[4..];

        let left_node: NodeId;
        let right_node: NodeId;
        if let Some(ln) = hash.get(first) {
            left_node = *ln;
        }else {
            left_node = arena.new_node(first);
            hash.insert(first, left_node);
        }
        if let Some(rn) = hash.get(second) {
            right_node = *rn;
        } else {
            right_node = arena.new_node(second);
            hash.insert(second, right_node);
        }

        left_node.append(right_node, &mut arena);
    }
    (hash, arena)
}

fn part_1(hash: &HashMap<&str, NodeId>, arena: &Arena<&str>) {
    let orbits: usize = hash.values()
        .map(|n| n.ancestors(arena).count() - 1)
        .sum();

    println!("Part 1: {} orbits", orbits);
}

fn part_2(hash: &HashMap<&str, NodeId>, arena: &Arena<&str>) {
    let me = hash.get("YOU").unwrap();
    let santa = hash.get("SAN").unwrap();

    let me_steps = me.ancestors(arena).skip(1).enumerate();

    //collect the second one because otherwise we could be enumerating it over and over
    let santa_steps = santa.ancestors(arena).skip(1).collect::<Vec<NodeId>>();

    for (i, s) in me_steps {
        for j in 0..santa_steps.len() {
            if s == santa_steps[j] {
                println!("Found match at {}", arena.get(s).unwrap().get());
                println!("Part 2: {}", i + j);
                return;
            }
        }
    }
}
